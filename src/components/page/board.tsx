import { useParams } from 'react-router-dom';
import React from 'react';
import { useQuery } from '@tanstack/react-query';
import { API, graphqlOperation } from 'aws-amplify';
import { columnsByBoardIDAndOrder } from '../../graphql/queries';
import Column from '../organisms/column';
import TaskCard from '../molecules/taskCard';
import Button from '../atoms/button';
import useShiftTask from '../../hooks/useShiftTask';
import { Column as ColumnType, Board as BoardType } from '../../models';
import { GraphQLQuery } from '@aws-amplify/api';
import EditBoardButton from '../molecules/editBoardButton';
import { UserType } from '../../interfaces';
import useModal from '../../hooks/useModal';
import CreateColumnModal from '../molecules/createColumnModal';

interface BoardProps {
  boardData: BoardType[];
  user: UserType;
}

export default function Board({ boardData, user }: BoardProps) {
  const params = useParams();
  const boardID: string = params.boardID || '';
  const { pushModal, popModal } = useModal();

  const { data: columnData = [] } = useQuery({
    queryKey: [boardID],
    queryFn: async (): Promise<ColumnType[]> => {
      return await API.graphql<GraphQLQuery<any>>(
        graphqlOperation(columnsByBoardIDAndOrder, { boardID })
      ).then(res => res.data.columnsByBoardIDAndOrder.items || []);
    }
  });

  const board = boardData.find((board: BoardType) => board.id === boardID);

  const {
    mousePos,
    setMousePos,
    dragging,
    setDragging,
    shiftTask
  } = useShiftTask();

  return (
    <>
      <div
        onMouseUp={() => {
          setDragging(null);
        }}
        onMouseLeave={() => {
          setDragging(null);
        }}
        onMouseDown={event => {
          setMousePos({ x: event.clientX, y: event.clientY });
        }}
        onMouseMove={event => {
          if (dragging) {
            setMousePos({ x: event.clientX, y: event.clientY });
          }
        }}
        className='grow flex flex-col overflow-hidden select-none	'
      >
        {board && board.name && (
          <div className='m-1'>
            <div className='flex items-center justify-between'>
              <h1 className='text-xl font-semibold m-1'>{board.name}</h1>
              <EditBoardButton board={board} user={user}></EditBoardButton>
            </div>

            <Button
              classNameAdd='bg-slate-300 font-semibold'
              onClick={() =>
                pushModal(
                  <CreateColumnModal
                    key='create column modal'
                    board={board}
                    close={popModal}
                  ></CreateColumnModal>
                )
              }
            >
              Create column
            </Button>
          </div>
        )}
        <div className='bg-slate-100 mx-1 grow flex flex-row overflow-x-scroll'>
          {columnData?.map((column: ColumnType, i: number) => (
            <Column
              key={column.id}
              column={column}
              setDragging={setDragging}
              shiftTask={shiftTask}
              last={i === columnData.length - 1}
              first={i === 0}
              dragging={dragging}
            ></Column>
          ))}
        </div>
      </div>
      {dragging && (
        <div
          style={{
            transform: `translate3d(${mousePos.x + dragging.offset.x}px,${
              mousePos.y + dragging.offset.y
            }px,0)`
          }}
          className='absolute top-0 left-0 pointer-events-none'
        >
          <TaskCard
            task={dragging.task}
            handleDraggingCurrent={() => {}}
            dragging={dragging}
            dragMoving={true}
            blocked={dragging.task.blocked || false}
          ></TaskCard>
        </div>
      )}
    </>
  );
}
