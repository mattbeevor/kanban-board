import React from 'react';
import Button from '../atoms/button';
import BoardListItem from '../atoms/boardListItem';
import useDeleteBoard from '../../hooks/useDeleteBoard';
import useCreateBoard from '../../hooks/useCreateBoard';
import { Board } from '../../models/index';
import useModal from '../../hooks/useModal';
import CreateBoardModal from '../molecules/createBoardModal';

interface HomeProps {
  user: { id: string; username: string };
  boardData: Board[];
}

export default function Home({ user, boardData }: HomeProps): JSX.Element {
  const deleteBoard = useDeleteBoard({ user });
  const { pushModal, popModal } = useModal();

  return (
    <div className='flex flex-col items-center'>
      <div className='m-2 w-3/4'>
        <h1 className='m-1 text-xl font-semibold font'>
          {boardData?.length === 0 ? '' : 'Boards:'}
        </h1>
        <>
          {boardData?.map((board: Board) => (
            <BoardListItem
              user={user}
              key={board.id}
              deleteBoard={deleteBoard}
              board={board}
            ></BoardListItem>
          ))}
        </>
      </div>
      <div className='mx-2'>
        <Button
          classNameAdd='bg-green-300 font-semibold'
          onClick={() =>
            pushModal(
              <CreateBoardModal
                key='create-board-modal'
                close={popModal}
                user={user}
              ></CreateBoardModal>
            )
          }
        >
          Create Board
        </Button>
      </div>
    </div>
  );
}
