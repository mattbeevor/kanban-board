import Button from '../atoms/button';
import React, { useState, ReactNode } from 'react';
import ConfirmModal from '../atoms/confirmModal';

interface DeleteButtonProps {
  deleteFunc: () => void;
  name: string;
  textOverride?: ReactNode;
}

export default function DeleteButton({
  deleteFunc,
  name,
  textOverride
}: DeleteButtonProps) {
  const [confirm, setConfirm] = useState(false);
  const text = textOverride || <>&#10006;</>;

  return (
    <>
      <Button
        classNameAdd='text-sm font-semibold bg-red-300 text-white'
        onClick={() => setConfirm(true)}
      >
        {text}
      </Button>
      {confirm && (
        <ConfirmModal
          confirm={deleteFunc}
          cancel={() => setConfirm(false)}
          question={`Delete ${name}?`}
        ></ConfirmModal>
      )}
    </>
  );
}
