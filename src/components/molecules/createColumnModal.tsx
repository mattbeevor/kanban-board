import React from 'react';
import ColumnEdit from './columnEdit';
import useCreateColumn from '../../hooks/useCreateColumn';
import { ColumnFields } from '../../interfaces';
import ModalWithConfirmExit from '../atoms/modalWithConfirmExit';
import { Board } from '../../models/index';

interface CreateColumnModalProps {
  close: () => void;
  board: Board;
}

export default function CreateColumnModal({
  close,
  board
}: CreateColumnModalProps) {
  const createColumn = useCreateColumn();

  const confirm = (columnFields: ColumnFields) => {
    createColumn.mutate({ columnFields, board });
    close();
  };

  return (
    <ModalWithConfirmExit
      shouldAsk={true}
      question='Discard column?'
      close={close}
    >
      <ColumnEdit confirmText={'Create'} confirm={confirm}></ColumnEdit>
    </ModalWithConfirmExit>
  );
}
