import React, { useState } from 'react';
import { Board } from '../../models';
import BoardEdit from './boardEdit';
import useUpdateBoard from '../../hooks/useUpdateBoard';
import { BoardFields, UserType } from '../../interfaces';
import ModalWithConfirmExit from '../atoms/modalWithConfirmExit';

interface EditBoardModalProps {
  board: Board;
  close: () => void;
  user: UserType;
}

export default function EditBoardModal({
  close,
  board,
  user
}: EditBoardModalProps) {
  const updateBoard = useUpdateBoard();
  const [didEdit, setDidEdit] = useState(false);

  const fields: BoardFields = {
    name: board.name || ''
  };

  const confirm = (boardFields: BoardFields) => {
    updateBoard.mutate({ user, boardFields, board });
    close();
  };

  return (
    <ModalWithConfirmExit
      shouldAsk={didEdit}
      question='Discard changes?'
      close={close}
    >
      <BoardEdit
        setDidEdit={setDidEdit}
        fields={fields}
        confirmText={'Update board'}
        confirm={confirm}
      ></BoardEdit>
    </ModalWithConfirmExit>
  );
}
