import React, { useState } from 'react';
import { Task } from '../../models';
import TaskEdit from './taskEdit';
import useUpdateTask from '../../hooks/useUpdateTask';
import { TaskFields } from '../../interfaces';
import ModalWithConfirmExit from '../atoms/modalWithConfirmExit';

interface EditTaskModalProps {
  task: Task;
  close: () => void;
}

export default function EditTaskModal({ close, task }: EditTaskModalProps) {
  const updateTask = useUpdateTask();
  const [didEdit, setDidEdit] = useState(false);

  const fields: TaskFields = {
    name: task.name || '',
    effort: task.effort || 0,
    estimate: task.estimate || 0,
    description: task.description || '',
    blocked: task.blocked || false
  };

  const confirm = (taskFields: TaskFields) => {
    updateTask.mutate({ taskFields, task });
    close();
  };

  return (
    <ModalWithConfirmExit
      shouldAsk={didEdit}
      question='Discard changes?'
      close={close}
      classNameAdd='w-1/2'
    >
      <TaskEdit
        setDidEdit={setDidEdit}
        fields={fields}
        confirmText={'Update task'}
        confirm={confirm}
      ></TaskEdit>
    </ModalWithConfirmExit>
  );
}
