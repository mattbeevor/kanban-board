import React, { useState } from 'react';
import Button from '../atoms/button';
import { ColumnFields } from '../../interfaces';
import InputEdit from './inputEdit';
import { inputType } from '../../enums';
import InfoModal from '../atoms/infoModal';
import useModal from '../../hooks/useModal';

interface ColumnEditProps {
  fields?: ColumnFields;
  confirm: (columnFields: ColumnFields) => void;
  confirmText: string;
  setDidEdit?: (arg: boolean) => void;
}

export default function ColumnEdit({
  fields,
  confirm,
  confirmText,
  setDidEdit
}: ColumnEditProps) {
  const [name, setName] = useState(fields?.name || '');
  const { pushModal, popModal } = useModal();

  const validateAndConfirm = () => {
    if (name === '') {
      pushModal(
        <InfoModal
          key='info-empty-title'
          text={'Title can not be empty'}
          close={popModal}
        ></InfoModal>
      );
    } else {
      confirm({
        name
      });
    }
  };

  return (
    <div>
      <div className='grid grid-cols-2 m-2'>
        <InputEdit
          name='Title'
          setter={setName}
          value={name}
          type={inputType.text}
          setDidEdit={setDidEdit}
          onEnter={validateAndConfirm}
        ></InputEdit>
      </div>
      <div className='flex justify-center m-2'>
        <Button
          classNameAdd='font-semibold bg-sky-100'
          onClick={validateAndConfirm}
        >
          {confirmText}
        </Button>
      </div>
    </div>
  );
}
