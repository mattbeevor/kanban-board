import React, { ChangeEvent } from 'react';
import { inputType } from '../../enums';

interface InputEditProps {
  name: string;
  value: any;
  setter: (value: any) => void;
  type: inputType;
  setDidEdit?: (value: boolean) => void;
  onEnter?: () => void;
}

export default function InputEdit({
  name,
  type,
  value,
  setter,
  setDidEdit,
  onEnter
}: InputEditProps) {
  const labelClassName = 'text-l font-semibold m-1 my-1 outline-0 ';
  const inputClassName =
    'text-l font-semibold m-1 my-1 outline-0  box-border rounded bg-slate-100 ';

  const label = `${name}:`;

  const handleChange = (value: any) => {
    setDidEdit && setDidEdit(true);
    setter(value);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter') {
      onEnter && onEnter();
    }
  };

  return (
    <>
      {type === inputType.text && (
        <>
          <label className={labelClassName + 'col-span-2'}>{label}</label>
          <input
            autoFocus
            className={inputClassName + 'col-span-2'}
            type='text'
            value={value}
            onChange={(event: ChangeEvent<HTMLInputElement>) =>
              handleChange(event.target.value)
            }
            onKeyDown={handleKeyDown}
          />
        </>
      )}
      {type === inputType.textarea && (
        <>
          <label className={labelClassName + 'col-span-2'}>{label}</label>
          <textarea
            className={inputClassName + 'h-32 col-span-2'}
            value={value}
            onChange={(event: ChangeEvent<HTMLTextAreaElement>) =>
              handleChange(event.target.value)
            }
          />
        </>
      )}
      {type === inputType.number && (
        <>
          <label className={labelClassName}>{label}</label>
          <input
            className={inputClassName}
            type='number'
            min='0'
            max='100'
            value={value}
            onChange={(event: ChangeEvent<HTMLInputElement>) =>
              handleChange(event.target.value)
            }
            onKeyDown={handleKeyDown}
          />
        </>
      )}
      {type === inputType.boolean && (
        <>
          <label className={labelClassName}>{label}</label>
          <input
            className='justify-self-start'
            type='checkbox'
            id='checkbox'
            checked={value}
            onChange={() => handleChange(!value)}
          />
        </>
      )}
    </>
  );
}
