import React from 'react';
import { Column } from '../../models';
import TaskEdit from './taskEdit';
import useCreateTask from '../../hooks/useCreateTask';
import { TaskFields } from '../../interfaces';
import ModalWithConfirmExit from '../atoms/modalWithConfirmExit';

interface CreateTaskModalProps {
  column: Column;
  close: () => void;
  order: number;
}

export default function CreateTaskModal({
  close,
  column,
  order
}: CreateTaskModalProps) {
  const createTask = useCreateTask();

  const confirm = (taskFields: TaskFields) => {
    createTask.mutate({ taskFields, order, column });
    close();
  };

  return (
    <ModalWithConfirmExit
      shouldAsk={true}
      question='Discard task?'
      close={close}
    >
      <TaskEdit confirmText={'Create'} confirm={confirm}></TaskEdit>
    </ModalWithConfirmExit>
  );
}
