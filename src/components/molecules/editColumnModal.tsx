import React, { useState } from 'react';
import { Column } from '../../models';
import ColumnEdit from './columnEdit';
import useUpdateColumn from '../../hooks/useUpdateColumn';
import { ColumnFields } from '../../interfaces';
import ModalWithConfirmExit from '../atoms/modalWithConfirmExit';

interface EditColumnModalProps {
  column: Column;
  close: () => void;
}

export default function EditColumnModal({
  close,
  column
}: EditColumnModalProps) {
  const updateColumn = useUpdateColumn();
  const [didEdit, setDidEdit] = useState(false);

  const fields: ColumnFields = {
    name: column.name || ''
  };

  const confirm = (columnFields: ColumnFields) => {
    updateColumn.mutate({ columnFields, column });
    close();
  };

  return (
    <ModalWithConfirmExit
      shouldAsk={didEdit}
      question='Discard changes?'
      close={close}
    >
      <ColumnEdit
        setDidEdit={setDidEdit}
        fields={fields}
        confirmText={'Update column'}
        confirm={confirm}
      ></ColumnEdit>
    </ModalWithConfirmExit>
  );
}
