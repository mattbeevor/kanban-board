import React, { useState } from 'react';
import Button from '../atoms/button';
import { TaskFields } from '../../interfaces';
import InputEdit from './inputEdit';
import { inputType } from '../../enums';
import InfoModal from '../atoms/infoModal';
import useModal from '../../hooks/useModal';

interface TaskEditProps {
  fields?: TaskFields;
  confirm: (taskFields: TaskFields) => void;
  confirmText: string;
  setDidEdit?: (arg: boolean) => void;
}

export default function TaskEdit({
  fields,
  confirm,
  confirmText,
  setDidEdit
}: TaskEditProps) {
  const [name, setName] = useState(fields?.name || '');
  const [description, setDescription] = useState(fields?.description || '');
  const [blocked, setBlocked] = useState(fields?.blocked || false);
  const [effort, setEffort] = useState(fields?.effort || 0);
  const [estimate, setEstimate] = useState(fields?.estimate || 0);
  const { pushModal, popModal } = useModal();

  const validateAndConfirm = () => {
    if (name === '') {
      pushModal(
        <InfoModal
          key='info-empty-title'
          text={'Title can not be empty'}
          close={popModal}
        ></InfoModal>
      );
    } else {
      confirm({
        name,
        description,
        blocked,
        estimate,
        effort
      });
    }
  };

  return (
    <div>
      <div className='grid grid-cols-2 m-2'>
        <InputEdit
          name='Title'
          setter={setName}
          value={name}
          type={inputType.text}
          setDidEdit={setDidEdit}
          onEnter={validateAndConfirm}
        ></InputEdit>
        <InputEdit
          name='Description'
          setter={setDescription}
          value={description}
          type={inputType.textarea}
          setDidEdit={setDidEdit}
        ></InputEdit>
        <InputEdit
          name='Blocked'
          setter={setBlocked}
          value={blocked}
          type={inputType.boolean}
          setDidEdit={setDidEdit}
        ></InputEdit>
        <InputEdit
          name='Required effort'
          setter={setEstimate}
          value={estimate}
          type={inputType.number}
          setDidEdit={setDidEdit}
        ></InputEdit>
        <InputEdit
          name='Effort completed'
          setter={setEffort}
          value={effort}
          type={inputType.number}
          setDidEdit={setDidEdit}
        ></InputEdit>
      </div>
      <div className='flex justify-center m-2'>
        <Button
          classNameAdd='font-semibold bg-sky-100'
          onClick={validateAndConfirm}
        >
          {confirmText}
        </Button>
      </div>
    </div>
  );
}
