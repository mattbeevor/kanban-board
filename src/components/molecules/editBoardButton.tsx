import React from 'react';
import useModal from '../../hooks/useModal';
import EditButton from '../molecules/editButton';
import EditBoardModal from '../molecules/editBoardModal';
import { UserType } from '../../interfaces';
import { Board } from '../../models';

interface Props {
  board: Board;
  user: UserType;
}

export default function EditBoardButton({ board, user }: Props) {
  const { pushModal, popModal } = useModal();

  return (
    <EditButton
      onClick={() => {
        pushModal(
          <EditBoardModal
            key='edit-board-modal'
            user={user}
            board={board}
            close={popModal}
          ></EditBoardModal>
        );
      }}
    ></EditButton>
  );
}
