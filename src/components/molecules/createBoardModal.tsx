import React from 'react';
import BoardEdit from './boardEdit';
import useCreateBoard from '../../hooks/useCreateBoard';
import { BoardFields, UserType } from '../../interfaces';
import ModalWithConfirmExit from '../atoms/modalWithConfirmExit';

interface CreateBoardModalProps {
  close: () => void;
  user: UserType;
}

export default function CreateBoardModal({
  close,
  user
}: CreateBoardModalProps) {
  const createBoard = useCreateBoard();

  const confirm = (boardFields: BoardFields) => {
    createBoard.mutate({ boardFields, user });
    close();
  };

  return (
    <ModalWithConfirmExit
      shouldAsk={true}
      question='Discard board?'
      close={close}
    >
      <BoardEdit confirmText={'Create'} confirm={confirm}></BoardEdit>
    </ModalWithConfirmExit>
  );
}
