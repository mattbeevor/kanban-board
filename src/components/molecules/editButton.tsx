import Button from '../atoms/button';
import React from 'react';

interface EditButtonProps {
  onClick: (open: boolean) => void;
}

export default function EditButton({ onClick = () => {} }: EditButtonProps) {
  return (
    <Button onClick={() => onClick(true)}>
      <svg className='w-8' viewBox='0 0 32 32'>
        <circle className='fill-black' cx='16' cy='16' r='2'></circle>
        <circle className='fill-black' cx='8' cy='16' r='2'></circle>
        <circle className='fill-black' cx='24' cy='16' r='2'></circle>
      </svg>
    </Button>
  );
}
