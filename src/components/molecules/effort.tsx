import React from 'react';
import useUpdateEffort from '../../hooks/useUpdateEffort';
import EffortNum from '../atoms/effortNum';
import { Task } from '../../models';

type Props = {
  effort: number;
  estimate: number;
  task: Task;
};

export default function Effort({ task, effort, estimate }: Props) {
  const updateEffort = useUpdateEffort();

  return (
    <div className='p-1'>
      <div>
        <div className='flex justify-between	'>
          <div>
            {estimate > 0 && (
              <>
                <span>Done:</span>
                <EffortNum
                  num={effort}
                  update={num =>
                    updateEffort.mutate({ task, effort: num, estimate })
                  }
                ></EffortNum>
              </>
            )}
          </div>
          <div>
            <span>Effort:</span>
            <EffortNum
              num={estimate}
              update={num =>
                updateEffort.mutate({ task, estimate: num, effort })
              }
            ></EffortNum>
          </div>
        </div>
        {estimate > 0 && (
          <div
            className={`w-full mt-1 h-2 ${
              estimate === 0 ? '' : 'bg-gray-200'
            } rounded-full cursor-pointer`}
            onClick={() =>
              updateEffort.mutate({ task, estimate, effort: effort + 1 })
            }
          >
            <div
              onClick={e => {
                updateEffort.mutate({ task, estimate, effort: effort - 1 });
                e.stopPropagation();
              }}
              className='h-2 bg-blue-500 rounded-full'
              style={{
                width:
                  estimate === 0
                    ? '0%'
                    : `${Math.min(100, (effort * 100) / estimate)}%`
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
}
