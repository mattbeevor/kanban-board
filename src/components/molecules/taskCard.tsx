import React from 'react';
import Button from '../atoms/button';
import DeleteButton from './deleteButton';
import useDeleteTask from '../../hooks/useDeleteTask';
import { Task } from '../../models';
import Effort from './effort';
import Blocked from '../atoms/blocked';
import EditButton from './editButton';

interface TaskCardProps {
  renameTask?: (params: { name: string; id: string }) => void;
  task: Task;
  onMouseUp?: (event: any) => void;
  editClick?: (open: boolean) => void;
  handleDraggingCurrent: (event: any) => void;
  dragging: { task: { id: string } } | null;
  dragMoving: boolean;
  blocked: boolean;
}

export default function TaskCard({
  task,
  onMouseUp,
  editClick = () => {},
  handleDraggingCurrent,
  dragging,
  dragMoving,
  blocked
}: TaskCardProps) {
  const isDraggedOver = dragging && dragging.task.id !== task.id;
  const isDragged = !dragMoving && dragging && dragging.task.id === task.id;
  const deleteTask = useDeleteTask();

  return (
    <div className={`w-56 ${isDraggedOver ? 'hover:pt-10' : ''}`}>
      <div
        className={`p-1 rounded task border-solid border-slate-400 border-2 bg-white ${
          isDragged ? 'hidden' : ''
        }`}
        onMouseUp={onMouseUp}
      >
        <div className='flex justify-between'>
          <Button
            onClick={editClick}
            className='hover:underline text-xl font-semibold m-1'
          >
            {task.name}
          </Button>
          <EditButton onClick={editClick}></EditButton>
        </div>
        <Effort
          task={task}
          estimate={task.estimate || 0}
          effort={task.effort || 0}
        ></Effort>
        <div className='flex items-end justify-between'>
          <Button onMouseDown={handleDraggingCurrent}>
            <svg className='w-8 h-8' viewBox='0 0 9 9'>
              <path d='M 0.5 4.5 l 1.5 -1.5 v 1 h 2 v -2 h -1 l 1.5 -1.5 l 1.5 1.5 h -1 v 2 h 2 v-1 l 1.5 1.5 l -1.5 1.5 v-1 h -2 v 2 h 1 l -1.5 1.5 l -1.5 -1.5 h 1 v -2 h -2 v 1 Z' />
            </svg>
          </Button>
          <div className='flex items-center'>
            <Blocked task={task} blocked={blocked}></Blocked>
            <DeleteButton
              deleteFunc={() => deleteTask.mutate({ task })}
              name={task.name}
            ></DeleteButton>
          </div>
        </div>
      </div>
    </div>
  );
}
