import EditTaskModal from '../molecules/editTaskModal';
import TaskCard from '../molecules/taskCard';
import React, { MouseEvent } from 'react';
import { Column, Task as TaskType } from '../../models';
import {
  shiftTaskFunctionType,
  setDraggingFunctionType,
  draggingType
} from '../../interfaces';
import useModal from '../../hooks/useModal';

interface TaskParams {
  task: TaskType;
  tasks: TaskType[];
  column: Column;
  setDragging: setDraggingFunctionType;
  shiftTask: shiftTaskFunctionType;
  dragging: draggingType;
}

export default function Task({
  task,
  tasks,
  setDragging,
  shiftTask,
  column,
  dragging
}: TaskParams) {
  const { pushModal, popModal } = useModal();

  const handleDraggingCurrent = (event: MouseEvent<HTMLButtonElement>) => {
    const targetElement = event.target as HTMLElement;
    const closest = targetElement?.closest('.task') || targetElement;
    const rect = closest.getBoundingClientRect();

    setDragging({
      task,
      tasks,
      offset: { x: rect.left - event.clientX, y: rect.top - event.clientY }
    });
  };

  return (
    <>
      <TaskCard
        onMouseUp={event => {
          event.stopPropagation();
          shiftTask({
            targetColumn: column,
            targetTask: task,
            tasks
          });
        }}
        task={task}
        editClick={() =>
          pushModal(
            <EditTaskModal
              key={task.id + 'editTask'}
              close={popModal}
              task={task}
            ></EditTaskModal>
          )
        }
        handleDraggingCurrent={handleDraggingCurrent}
        dragging={dragging}
        blocked={task.blocked}
        dragMoving={false}
      ></TaskCard>
    </>
  );
}
