import React from 'react';
import { Outlet } from 'react-router-dom';
import Button from '../atoms/button';

interface NavProps {
  signOut: () => void;
}

export default function Nav({ signOut }: NavProps) {
  return (
    <main className='h-screen max-h-full flex flex-col	'>
      <nav className='bg-slate-200 flex justify-between'>
        <Button classNameAdd='font-semibold text-xl' to={`/`}>
          Home
        </Button>
        <Button onClick={signOut}>Sign out</Button>
      </nav>
      <Outlet></Outlet>
    </main>
  );
}
