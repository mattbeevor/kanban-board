import { API, graphqlOperation } from 'aws-amplify';
import { useQuery } from '@tanstack/react-query';
import { tasksByColumnIDAndOrder } from '../../graphql/queries';
import Task from './task';
import Button from '../atoms/button';
import React from 'react';
import DeleteButton from '../molecules/deleteButton';
import useDeleteColumn from '../../hooks/useDeleteColumn';
import useMoveColumn from '../../hooks/useMoveColumn';
import { Task as TaskType, Column as ColumnType } from '../../models';
import {
  shiftTaskFunctionType,
  setDraggingFunctionType,
  draggingType
} from '../../interfaces';
import { GraphQLQuery } from '@aws-amplify/api';
import CreateTaskModal from '../molecules/createTaskModal';
import useModal from '../../hooks/useModal';
import EditButton from '../molecules/editButton';
import EditColumnModal from '../molecules/editColumnModal';

interface ColumnProps {
  column: ColumnType;
  setDragging: setDraggingFunctionType;
  shiftTask: shiftTaskFunctionType;
  last: boolean;
  first: boolean;
  dragging: draggingType;
}

export default function Column({
  column,
  setDragging,
  shiftTask,
  last,
  first,
  dragging
}: ColumnProps) {
  const { data: taskData } = useQuery({
    queryKey: [column.id],
    queryFn: async (): Promise<TaskType[]> =>
      await API.graphql<GraphQLQuery<any>>(
        graphqlOperation(tasksByColumnIDAndOrder, { columnID: column.id })
      ).then(res => res?.data?.tasksByColumnIDAndOrder?.items || [])
  });

  const { pushModal, popModal } = useModal();

  const moveColumn = useMoveColumn();
  const deleteColumn = useDeleteColumn();

  return (
    <div
      onMouseUp={event => {
        event.stopPropagation();
        shiftTask({
          targetColumn: column,
          tasks: taskData || []
        });
      }}
      className='w-56 shrink-0 flex flex-col bg-slate-100	 min-w-max overflow-y-hidden'
    >
      <div className='py-1 bg-slate-200'>
        <div className='flex justify-between items-center'>
          <h1 className='text-xl font-semibold m-1'>{column.name}</h1>
          <EditButton
            onClick={() => {
              pushModal(
                <EditColumnModal
                  key='edit-column-modal'
                  column={column}
                  close={popModal}
                ></EditColumnModal>
              );
            }}
          ></EditButton>
        </div>
        <div className='flex justify-between pr-2'>
          <Button
            onClick={() =>
              pushModal(
                <CreateTaskModal
                  key={column.id + 'createTaskModal'}
                  close={popModal}
                  column={column}
                  order={taskData?.length || 0}
                ></CreateTaskModal>
              )
            }
            classNameAdd='bg-slate-300 font-semibold'
          >
            Create task
          </Button>
          <div>
            <DeleteButton
              deleteFunc={() => deleteColumn.mutate({ column })}
              name={column.name}
            ></DeleteButton>
            <Button
              onClick={() =>
                moveColumn.mutate({
                  column,
                  direction: -1
                })
              }
              classNameAdd={
                first
                  ? 'px-1 bg-slate-100 cursor-not-allowed opacity-50	pointer-none'
                  : 'bg-slate-300'
              }
            >
              &larr;
            </Button>
            <Button
              onClick={() =>
                moveColumn.mutate({
                  column,
                  direction: 1
                })
              }
              classNameAdd={
                last
                  ? 'px-1 bg-slate-100 cursor-not-allowed opacity-50	pointer-none'
                  : 'bg-slate-300'
              }
            >
              &rarr;
            </Button>
          </div>
        </div>
      </div>

      <div className='overflow-y-scroll grow'>
        <div className='w-56'>
          {taskData?.map((task: TaskType) => (
            <Task
              key={task.id}
              task={task}
              tasks={taskData || []}
              setDragging={setDragging}
              shiftTask={shiftTask}
              column={column}
              dragging={dragging}
            ></Task>
          ))}
        </div>
      </div>
    </div>
  );
}
