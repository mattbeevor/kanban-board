import React from 'react';
import { API, graphqlOperation } from 'aws-amplify';
import '@aws-amplify/ui-react/styles.css';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import Home from '../page/home';
import Board from '../page/board';
import Nav from './nav';
import { listBoards } from '../../graphql/queries';
import { useQuery } from '@tanstack/react-query';
import { Board as BoardType } from '../../models';
import { GraphQLQuery } from '@aws-amplify/api';

interface RouterProps {
  user: { id: string; username: string };
  signOut: () => void;
}

export default function Router({ signOut, user }: RouterProps) {
  const { data: boardData = [] } = useQuery({
    queryKey: [user.username],
    queryFn: async (): Promise<BoardType[]> =>
      await API.graphql<GraphQLQuery<any>>(graphqlOperation(listBoards)).then(
        res => res.data?.listBoards?.items
      )
  });

  const router = createBrowserRouter([
    {
      element: <Nav signOut={signOut} />,
      path: '/',
      children: [
        {
          path: '/',
          element: <Home user={user} boardData={boardData}></Home>
        },
        {
          path: 'boards/:boardID',
          element: boardData ? (
            <Board user={user} boardData={boardData}></Board>
          ) : null
        }
      ]
    }
  ]);
  return <RouterProvider router={router} />;
}
