import React from 'react';
import Modal from '../atoms/modal';
import ConfirmModal from '../atoms/confirmModal';
import useModal from '../../hooks/useModal';

interface ModalWithConfirmExitProps {
  close: () => void;
  children: React.ReactNode;
  shouldAsk: boolean;
  question: string;
  classNameAdd?: string;
}

export default function ModalWithConfirmExit({
  close,
  children,
  shouldAsk,
  question,
  classNameAdd
}: ModalWithConfirmExitProps) {
  const { pushModal, popModal } = useModal();

  const exit = () => {
    if (shouldAsk) {
      pushModal(
        <ConfirmModal
          key={question}
          confirm={() => {
            popModal();
            close();
          }}
          cancel={popModal}
          question={question}
          cancelTextOverride={'Cancel'}
          confirmTextOverride={'Yes'}
        ></ConfirmModal>
      );
    } else {
      close();
    }
  };

  return (
    <Modal classNameAdd={classNameAdd} close={exit}>
      {children}
    </Modal>
  );
}
