import { Link as RouterLink, LinkProps } from 'react-router-dom';
import React from 'react';

interface Props extends LinkProps {
  children: React.ReactNode;
  to: string;
  classNameAdd?: string;
}

export default function Link({ to, children, classNameAdd = '' }: Props) {
  return (
    <RouterLink to={to} className={`hover:underline ${classNameAdd}`}>
      {children}
    </RouterLink>
  );
}
