import React, { ChangeEvent } from 'react';

type Props = {
  update: (num: number) => void;
  num: number;
};

export default function effortNum({ update, num }: Props) {
  return (
    <input
      className='w-10 text-l font-semibold m-1 my-1 outline-0  box-border rounded bg-slate-100'
      type='number'
      value={num}
      onChange={(event: ChangeEvent<HTMLInputElement>) => {
        update(parseInt(event.target.value));
      }}
      min='0'
      max='100'
    />
  );
}
