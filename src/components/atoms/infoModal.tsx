import React from 'react';
import Button from './button';
import Modal from './modal';

interface InfoModalProps {
  text: string;
  close: () => void;
}

export default function InfoModal({ text, close }: InfoModalProps) {
  return (
    <Modal close={close}>
      <p className='p-3'>{text}</p>
      <div className='flex justify-center'>
        <Button classNameAdd='font-semibold bg-sky-100' onClick={close}>
          Ok
        </Button>
      </div>
    </Modal>
  );
}
