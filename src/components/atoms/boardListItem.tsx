import React from 'react';
import DeleteButton from '../molecules/deleteButton';
import { Board } from '../../models';
import Link from './link';
import EditBoardButton from '../molecules/editBoardButton';
import { UserType } from '../../interfaces';

interface Props {
  board: Board;
  deleteBoard: { mutate: (params: { id: string }) => void };
  user: UserType;
}

export default function BoardListItem({ board, deleteBoard, user }: Props) {
  return (
    <div className='flex items-center justify-between'>
      <Link classNameAdd='text-xl font-semibold m-1' to={`boards/${board.id}`}>
        {board.name}
      </Link>
      <div className='flex items-center'>
        <EditBoardButton user={user} board={board}></EditBoardButton>
        <DeleteButton
          deleteFunc={() => deleteBoard.mutate({ id: board.id })}
          name={board.name}
        ></DeleteButton>
      </div>
    </div>
  );
}
