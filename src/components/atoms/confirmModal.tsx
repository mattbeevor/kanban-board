import React from 'react';
import Button from './button';
import Modal from './modal';

interface ConfirmModalProps {
  confirm: () => void;
  cancel: () => void;
  question: string;
  cancelTextOverride?: string;
  confirmTextOverride?: string;
}

export default function ConfirmModal({
  confirm,
  cancel,
  question,
  cancelTextOverride,
  confirmTextOverride
}: ConfirmModalProps) {
  return (
    <Modal close={cancel}>
      <p className='p-3'>{question}</p>
      <div className='flex justify-center'>
        <Button classNameAdd='font-semibold bg-sky-100' onClick={cancel}>
          {cancelTextOverride || 'Cancel'}
        </Button>
        <Button classNameAdd='font-semibold bg-sky-100' onClick={confirm}>
          {confirmTextOverride || 'Confirm'}
        </Button>
      </div>
    </Modal>
  );
}
