import React from 'react';
import Link from '../atoms/link';

type Props = {
  to?: string;
  children: React.ReactNode;
  classNameOverride?: string;
  classNameAdd?: string;
  [key: string]: any;
};

export default function Button({
  to,
  children,
  classNameOverride,
  classNameAdd,
  ...props
}: Props) {
  const className = `${
    classNameOverride ? classNameOverride : 'rounded m-1 px-1'
  } ${classNameAdd || ''}`;
  return to ? (
    <Link to={to} classNameAdd={className} {...props}>
      {children}
    </Link>
  ) : (
    <button className={className} {...props}>
      {children}
    </button>
  );
}
