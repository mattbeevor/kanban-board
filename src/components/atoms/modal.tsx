import React, { ReactNode } from 'react';

interface Props {
  children: ReactNode;
  close: () => void;
  classNameAdd?: string;
  classNameOverride?: string;
}

export default function Modal({
  children,
  close,
  classNameAdd,
  classNameOverride
}: Props) {
  const className = `${
    classNameOverride ? classNameOverride : 'p-2 rounded bg-white flex flex-col'
  } ${classNameAdd || ''}`;

  return (
    <div
      className='fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-10'
      onClick={close}
    >
      <div className={className} onClick={event => event.stopPropagation()}>
        {children}
      </div>
    </div>
  );
}
