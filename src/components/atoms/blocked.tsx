import React from 'react';
import useToggleBlocked from '../../hooks/useToggleBlocked';
import Button from './button';
import { Task } from '../../models';

type Props = {
  blocked: boolean;
  task: Task;
};

export default function Blocked({ task, blocked }: Props) {
  const toggleBlocked = useToggleBlocked();

  return (
    <Button onClick={() => toggleBlocked.mutate({ task })}>
      <svg className='stroke-1 w-6 h-6 fill-none' viewBox='0 0 6 6'>
        <circle
          className={`${
            blocked === true ? 'stroke-red-400' : 'stroke-slate-200'
          }`}
          cx='3'
          cy='3'
          r='2.5'
        />
        <path
          className={`${
            blocked === true ? 'stroke-red-400' : 'stroke-slate-200'
          }`}
          d='M 1 1 L 5 5'
        />
      </svg>
    </Button>
  );
}
