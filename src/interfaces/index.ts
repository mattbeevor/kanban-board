import { Column, Task } from '../models/index';

export type shiftTaskFunctionType = ({
  tasks,
  targetColumn,
  targetTask
}: {
  tasks: Task[];
  targetColumn: Column;
  targetTask?: Task;
}) => any;

export type setDraggingFunctionType = ({
  task,
  tasks,
  offset
}: {
  task: Task;
  tasks: Task[];
  offset: { x: number; y: number };
}) => void;

export type draggingType = {
  task: Task;
  tasks: Task[];
  offset: {
    x: number;
    y: number;
  };
} | null;

export type TaskFields = {
  name: string;
  effort: number;
  estimate: number;
  description: string;
  blocked: boolean;
};

export type ColumnFields = {
  name: string;
};

export type BoardFields = {
  name: string;
};

export type UserType = {
  username: string;
};
