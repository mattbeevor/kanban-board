/* tslint:disable */
/* eslint-disable */
//  This file was automatically generated and should not be edited.

export type CreateTaskInput = {
  id?: string | null,
  columnID: string,
  name: string,
  description: string,
  blocked: boolean,
  estimate?: number | null,
  effort?: number | null,
  order: number,
};

export type ModelTaskConditionInput = {
  columnID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  blocked?: ModelBooleanInput | null,
  estimate?: ModelIntInput | null,
  effort?: ModelIntInput | null,
  order?: ModelIntInput | null,
  and?: Array< ModelTaskConditionInput | null > | null,
  or?: Array< ModelTaskConditionInput | null > | null,
  not?: ModelTaskConditionInput | null,
};

export type ModelIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export enum ModelAttributeTypes {
  binary = "binary",
  binarySet = "binarySet",
  bool = "bool",
  list = "list",
  map = "map",
  number = "number",
  numberSet = "numberSet",
  string = "string",
  stringSet = "stringSet",
  _null = "_null",
}


export type ModelSizeInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export type ModelStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
  size?: ModelSizeInput | null,
};

export type ModelBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type ModelIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  attributeExists?: boolean | null,
  attributeType?: ModelAttributeTypes | null,
};

export type Task = {
  __typename: "Task",
  id: string,
  columnID: string,
  name: string,
  description: string,
  blocked: boolean,
  estimate?: number | null,
  effort?: number | null,
  order: number,
  createdAt: string,
  updatedAt: string,
  owner?: string | null,
};

export type UpdateTaskInput = {
  id: string,
  columnID?: string | null,
  name?: string | null,
  description?: string | null,
  blocked?: boolean | null,
  estimate?: number | null,
  effort?: number | null,
  order?: number | null,
};

export type DeleteTaskInput = {
  id: string,
};

export type CreateBoardInput = {
  id?: string | null,
  name: string,
};

export type ModelBoardConditionInput = {
  name?: ModelStringInput | null,
  and?: Array< ModelBoardConditionInput | null > | null,
  or?: Array< ModelBoardConditionInput | null > | null,
  not?: ModelBoardConditionInput | null,
};

export type Board = {
  __typename: "Board",
  id: string,
  Columns?: ModelColumnConnection | null,
  name: string,
  createdAt: string,
  updatedAt: string,
  owner?: string | null,
};

export type ModelColumnConnection = {
  __typename: "ModelColumnConnection",
  items:  Array<Column | null >,
  nextToken?: string | null,
};

export type Column = {
  __typename: "Column",
  id: string,
  boardID: string,
  Tasks?: ModelTaskConnection | null,
  name: string,
  order: number,
  createdAt: string,
  updatedAt: string,
  owner?: string | null,
};

export type ModelTaskConnection = {
  __typename: "ModelTaskConnection",
  items:  Array<Task | null >,
  nextToken?: string | null,
};

export type UpdateBoardInput = {
  id: string,
  name?: string | null,
};

export type DeleteBoardInput = {
  id: string,
};

export type CreateColumnInput = {
  id?: string | null,
  boardID: string,
  name: string,
  order: number,
};

export type ModelColumnConditionInput = {
  boardID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  order?: ModelIntInput | null,
  and?: Array< ModelColumnConditionInput | null > | null,
  or?: Array< ModelColumnConditionInput | null > | null,
  not?: ModelColumnConditionInput | null,
};

export type UpdateColumnInput = {
  id: string,
  boardID?: string | null,
  name?: string | null,
  order?: number | null,
};

export type DeleteColumnInput = {
  id: string,
};

export type ModelTaskFilterInput = {
  id?: ModelIDInput | null,
  columnID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  description?: ModelStringInput | null,
  blocked?: ModelBooleanInput | null,
  estimate?: ModelIntInput | null,
  effort?: ModelIntInput | null,
  order?: ModelIntInput | null,
  and?: Array< ModelTaskFilterInput | null > | null,
  or?: Array< ModelTaskFilterInput | null > | null,
  not?: ModelTaskFilterInput | null,
};

export type ModelIntKeyConditionInput = {
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
};

export enum ModelSortDirection {
  ASC = "ASC",
  DESC = "DESC",
}


export type ModelBoardFilterInput = {
  id?: ModelIDInput | null,
  name?: ModelStringInput | null,
  and?: Array< ModelBoardFilterInput | null > | null,
  or?: Array< ModelBoardFilterInput | null > | null,
  not?: ModelBoardFilterInput | null,
};

export type ModelBoardConnection = {
  __typename: "ModelBoardConnection",
  items:  Array<Board | null >,
  nextToken?: string | null,
};

export type ModelColumnFilterInput = {
  id?: ModelIDInput | null,
  boardID?: ModelIDInput | null,
  name?: ModelStringInput | null,
  order?: ModelIntInput | null,
  and?: Array< ModelColumnFilterInput | null > | null,
  or?: Array< ModelColumnFilterInput | null > | null,
  not?: ModelColumnFilterInput | null,
};

export type ModelSubscriptionTaskFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  columnID?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  description?: ModelSubscriptionStringInput | null,
  blocked?: ModelSubscriptionBooleanInput | null,
  estimate?: ModelSubscriptionIntInput | null,
  effort?: ModelSubscriptionIntInput | null,
  order?: ModelSubscriptionIntInput | null,
  and?: Array< ModelSubscriptionTaskFilterInput | null > | null,
  or?: Array< ModelSubscriptionTaskFilterInput | null > | null,
};

export type ModelSubscriptionIDInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  in?: Array< string | null > | null,
  notIn?: Array< string | null > | null,
};

export type ModelSubscriptionStringInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
  in?: Array< string | null > | null,
  notIn?: Array< string | null > | null,
};

export type ModelSubscriptionBooleanInput = {
  ne?: boolean | null,
  eq?: boolean | null,
};

export type ModelSubscriptionIntInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  between?: Array< number | null > | null,
  in?: Array< number | null > | null,
  notIn?: Array< number | null > | null,
};

export type ModelSubscriptionBoardFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  and?: Array< ModelSubscriptionBoardFilterInput | null > | null,
  or?: Array< ModelSubscriptionBoardFilterInput | null > | null,
};

export type ModelSubscriptionColumnFilterInput = {
  id?: ModelSubscriptionIDInput | null,
  boardID?: ModelSubscriptionIDInput | null,
  name?: ModelSubscriptionStringInput | null,
  order?: ModelSubscriptionIntInput | null,
  and?: Array< ModelSubscriptionColumnFilterInput | null > | null,
  or?: Array< ModelSubscriptionColumnFilterInput | null > | null,
};

export type CreateTaskMutationVariables = {
  input: CreateTaskInput,
  condition?: ModelTaskConditionInput | null,
};

export type CreateTaskMutation = {
  createTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type UpdateTaskMutationVariables = {
  input: UpdateTaskInput,
  condition?: ModelTaskConditionInput | null,
};

export type UpdateTaskMutation = {
  updateTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type DeleteTaskMutationVariables = {
  input: DeleteTaskInput,
  condition?: ModelTaskConditionInput | null,
};

export type DeleteTaskMutation = {
  deleteTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type CreateBoardMutationVariables = {
  input: CreateBoardInput,
  condition?: ModelBoardConditionInput | null,
};

export type CreateBoardMutation = {
  createBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type UpdateBoardMutationVariables = {
  input: UpdateBoardInput,
  condition?: ModelBoardConditionInput | null,
};

export type UpdateBoardMutation = {
  updateBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type DeleteBoardMutationVariables = {
  input: DeleteBoardInput,
  condition?: ModelBoardConditionInput | null,
};

export type DeleteBoardMutation = {
  deleteBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type CreateColumnMutationVariables = {
  input: CreateColumnInput,
  condition?: ModelColumnConditionInput | null,
};

export type CreateColumnMutation = {
  createColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type UpdateColumnMutationVariables = {
  input: UpdateColumnInput,
  condition?: ModelColumnConditionInput | null,
};

export type UpdateColumnMutation = {
  updateColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type DeleteColumnMutationVariables = {
  input: DeleteColumnInput,
  condition?: ModelColumnConditionInput | null,
};

export type DeleteColumnMutation = {
  deleteColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type GetTaskQueryVariables = {
  id: string,
};

export type GetTaskQuery = {
  getTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type ListTasksQueryVariables = {
  filter?: ModelTaskFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListTasksQuery = {
  listTasks?:  {
    __typename: "ModelTaskConnection",
    items:  Array< {
      __typename: "Task",
      id: string,
      columnID: string,
      name: string,
      description: string,
      blocked: boolean,
      estimate?: number | null,
      effort?: number | null,
      order: number,
      createdAt: string,
      updatedAt: string,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type TasksByColumnIDAndOrderQueryVariables = {
  columnID: string,
  order?: ModelIntKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelTaskFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type TasksByColumnIDAndOrderQuery = {
  tasksByColumnIDAndOrder?:  {
    __typename: "ModelTaskConnection",
    items:  Array< {
      __typename: "Task",
      id: string,
      columnID: string,
      name: string,
      description: string,
      blocked: boolean,
      estimate?: number | null,
      effort?: number | null,
      order: number,
      createdAt: string,
      updatedAt: string,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetBoardQueryVariables = {
  id: string,
};

export type GetBoardQuery = {
  getBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type ListBoardsQueryVariables = {
  filter?: ModelBoardFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListBoardsQuery = {
  listBoards?:  {
    __typename: "ModelBoardConnection",
    items:  Array< {
      __typename: "Board",
      id: string,
      Columns?:  {
        __typename: "ModelColumnConnection",
        items:  Array< {
          __typename: "Column",
          id: string,
          boardID: string,
          Tasks?:  {
            __typename: "ModelTaskConnection",
            items:  Array< {
              __typename: "Task",
              id: string,
              columnID: string,
              name: string,
              description: string,
              blocked: boolean,
              estimate?: number | null,
              effort?: number | null,
              order: number,
              createdAt: string,
              updatedAt: string,
              owner?: string | null,
            } | null >,
            nextToken?: string | null,
          } | null,
          name: string,
          order: number,
          createdAt: string,
          updatedAt: string,
          owner?: string | null,
        } | null >,
        nextToken?: string | null,
      } | null,
      name: string,
      createdAt: string,
      updatedAt: string,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type GetColumnQueryVariables = {
  id: string,
};

export type GetColumnQuery = {
  getColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type ListColumnsQueryVariables = {
  filter?: ModelColumnFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListColumnsQuery = {
  listColumns?:  {
    __typename: "ModelColumnConnection",
    items:  Array< {
      __typename: "Column",
      id: string,
      boardID: string,
      Tasks?:  {
        __typename: "ModelTaskConnection",
        items:  Array< {
          __typename: "Task",
          id: string,
          columnID: string,
          name: string,
          description: string,
          blocked: boolean,
          estimate?: number | null,
          effort?: number | null,
          order: number,
          createdAt: string,
          updatedAt: string,
          owner?: string | null,
        } | null >,
        nextToken?: string | null,
      } | null,
      name: string,
      order: number,
      createdAt: string,
      updatedAt: string,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type ColumnsByBoardIDAndOrderQueryVariables = {
  boardID: string,
  order?: ModelIntKeyConditionInput | null,
  sortDirection?: ModelSortDirection | null,
  filter?: ModelColumnFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ColumnsByBoardIDAndOrderQuery = {
  columnsByBoardIDAndOrder?:  {
    __typename: "ModelColumnConnection",
    items:  Array< {
      __typename: "Column",
      id: string,
      boardID: string,
      Tasks?:  {
        __typename: "ModelTaskConnection",
        items:  Array< {
          __typename: "Task",
          id: string,
          columnID: string,
          name: string,
          description: string,
          blocked: boolean,
          estimate?: number | null,
          effort?: number | null,
          order: number,
          createdAt: string,
          updatedAt: string,
          owner?: string | null,
        } | null >,
        nextToken?: string | null,
      } | null,
      name: string,
      order: number,
      createdAt: string,
      updatedAt: string,
      owner?: string | null,
    } | null >,
    nextToken?: string | null,
  } | null,
};

export type OnCreateTaskSubscriptionVariables = {
  filter?: ModelSubscriptionTaskFilterInput | null,
  owner?: string | null,
};

export type OnCreateTaskSubscription = {
  onCreateTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnUpdateTaskSubscriptionVariables = {
  filter?: ModelSubscriptionTaskFilterInput | null,
  owner?: string | null,
};

export type OnUpdateTaskSubscription = {
  onUpdateTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnDeleteTaskSubscriptionVariables = {
  filter?: ModelSubscriptionTaskFilterInput | null,
  owner?: string | null,
};

export type OnDeleteTaskSubscription = {
  onDeleteTask?:  {
    __typename: "Task",
    id: string,
    columnID: string,
    name: string,
    description: string,
    blocked: boolean,
    estimate?: number | null,
    effort?: number | null,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnCreateBoardSubscriptionVariables = {
  filter?: ModelSubscriptionBoardFilterInput | null,
  owner?: string | null,
};

export type OnCreateBoardSubscription = {
  onCreateBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnUpdateBoardSubscriptionVariables = {
  filter?: ModelSubscriptionBoardFilterInput | null,
  owner?: string | null,
};

export type OnUpdateBoardSubscription = {
  onUpdateBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnDeleteBoardSubscriptionVariables = {
  filter?: ModelSubscriptionBoardFilterInput | null,
  owner?: string | null,
};

export type OnDeleteBoardSubscription = {
  onDeleteBoard?:  {
    __typename: "Board",
    id: string,
    Columns?:  {
      __typename: "ModelColumnConnection",
      items:  Array< {
        __typename: "Column",
        id: string,
        boardID: string,
        Tasks?:  {
          __typename: "ModelTaskConnection",
          items:  Array< {
            __typename: "Task",
            id: string,
            columnID: string,
            name: string,
            description: string,
            blocked: boolean,
            estimate?: number | null,
            effort?: number | null,
            order: number,
            createdAt: string,
            updatedAt: string,
            owner?: string | null,
          } | null >,
          nextToken?: string | null,
        } | null,
        name: string,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnCreateColumnSubscriptionVariables = {
  filter?: ModelSubscriptionColumnFilterInput | null,
  owner?: string | null,
};

export type OnCreateColumnSubscription = {
  onCreateColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnUpdateColumnSubscriptionVariables = {
  filter?: ModelSubscriptionColumnFilterInput | null,
  owner?: string | null,
};

export type OnUpdateColumnSubscription = {
  onUpdateColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};

export type OnDeleteColumnSubscriptionVariables = {
  filter?: ModelSubscriptionColumnFilterInput | null,
  owner?: string | null,
};

export type OnDeleteColumnSubscription = {
  onDeleteColumn?:  {
    __typename: "Column",
    id: string,
    boardID: string,
    Tasks?:  {
      __typename: "ModelTaskConnection",
      items:  Array< {
        __typename: "Task",
        id: string,
        columnID: string,
        name: string,
        description: string,
        blocked: boolean,
        estimate?: number | null,
        effort?: number | null,
        order: number,
        createdAt: string,
        updatedAt: string,
        owner?: string | null,
      } | null >,
      nextToken?: string | null,
    } | null,
    name: string,
    order: number,
    createdAt: string,
    updatedAt: string,
    owner?: string | null,
  } | null,
};
