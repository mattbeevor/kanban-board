/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const getTask = /* GraphQL */ `
  query GetTask($id: ID!) {
    getTask(id: $id) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listTasks = /* GraphQL */ `
  query ListTasks(
    $filter: ModelTaskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listTasks(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        columnID
        name
        description
        blocked
        estimate
        remaining
        effort
        order
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const tasksByColumnIDAndOrder = /* GraphQL -- edited to not get task descriptions*/ `
  query TasksByColumnIDAndOrder(
    $columnID: ID!
    $order: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelTaskFilterInput
    $limit: Int
    $nextToken: String
  ) {
    tasksByColumnIDAndOrder(
      columnID: $columnID
      order: $order
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        columnID
        name
        description
        blocked
        estimate
        remaining
        effort
        order
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getBoard = /* GraphQL */ `
  query GetBoard($id: ID!) {
    getBoard(id: $id) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listBoards = /* GraphQL -- edited to just list boards*/ `
  query ListBoards(
    $filter: ModelBoardFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listBoards(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        name
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const getColumn = /* GraphQL */ `
  query GetColumn($id: ID!) {
    getColumn(id: $id) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const listColumns = /* GraphQL */ `
  query ListColumns(
    $filter: ModelColumnFilterInput
    $limit: Int
    $nextToken: String
  ) {
    listColumns(filter: $filter, limit: $limit, nextToken: $nextToken) {
      items {
        id
        boardID
        Tasks {
          items {
            id
            columnID
            name
            description
            blocked
            estimate
            remaining
            effort
            order
            createdAt
            updatedAt
            owner
          }
          nextToken
        }
        name
        order
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
export const columnsByBoardIDAndOrder = /* GraphQL edited, don't refetch tasks */ `
  query ColumnsByBoardIDAndOrder(
    $boardID: ID!
    $order: ModelIntKeyConditionInput
    $sortDirection: ModelSortDirection
    $filter: ModelColumnFilterInput
    $limit: Int
    $nextToken: String
  ) {
    columnsByBoardIDAndOrder(
      boardID: $boardID
      order: $order
      sortDirection: $sortDirection
      filter: $filter
      limit: $limit
      nextToken: $nextToken
    ) {
      items {
        id
        boardID
        name
        order
        createdAt
        updatedAt
        owner
      }
      nextToken
    }
  }
`;
