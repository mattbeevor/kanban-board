/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const createTask = /* GraphQL */ `
  mutation CreateTask(
    $input: CreateTaskInput!
    $condition: ModelTaskConditionInput
  ) {
    createTask(input: $input, condition: $condition) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateTask = /* GraphQL */ `
  mutation UpdateTask(
    $input: UpdateTaskInput!
    $condition: ModelTaskConditionInput
  ) {
    updateTask(input: $input, condition: $condition) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteTask = /* GraphQL */ `
  mutation DeleteTask(
    $input: DeleteTaskInput!
    $condition: ModelTaskConditionInput
  ) {
    deleteTask(input: $input, condition: $condition) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createBoard = /* GraphQL */ `
  mutation CreateBoard(
    $input: CreateBoardInput!
    $condition: ModelBoardConditionInput
  ) {
    createBoard(input: $input, condition: $condition) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateBoard = /* GraphQL */ `
  mutation UpdateBoard(
    $input: UpdateBoardInput!
    $condition: ModelBoardConditionInput
  ) {
    updateBoard(input: $input, condition: $condition) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteBoard = /* GraphQL */ `
  mutation DeleteBoard(
    $input: DeleteBoardInput!
    $condition: ModelBoardConditionInput
  ) {
    deleteBoard(input: $input, condition: $condition) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const createColumn = /* GraphQL */ `
  mutation CreateColumn(
    $input: CreateColumnInput!
    $condition: ModelColumnConditionInput
  ) {
    createColumn(input: $input, condition: $condition) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const updateColumn = /* GraphQL */ `
  mutation UpdateColumn(
    $input: UpdateColumnInput!
    $condition: ModelColumnConditionInput
  ) {
    updateColumn(input: $input, condition: $condition) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const deleteColumn = /* GraphQL */ `
  mutation DeleteColumn(
    $input: DeleteColumnInput!
    $condition: ModelColumnConditionInput
  ) {
    deleteColumn(input: $input, condition: $condition) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
