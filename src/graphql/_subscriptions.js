/* eslint-disable */
// this is an auto generated file. This will be overwritten

export const onCreateTask = /* GraphQL */ `
  subscription OnCreateTask(
    $filter: ModelSubscriptionTaskFilterInput
    $owner: String
  ) {
    onCreateTask(filter: $filter, owner: $owner) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateTask = /* GraphQL */ `
  subscription OnUpdateTask(
    $filter: ModelSubscriptionTaskFilterInput
    $owner: String
  ) {
    onUpdateTask(filter: $filter, owner: $owner) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteTask = /* GraphQL */ `
  subscription OnDeleteTask(
    $filter: ModelSubscriptionTaskFilterInput
    $owner: String
  ) {
    onDeleteTask(filter: $filter, owner: $owner) {
      id
      columnID
      name
      description
      blocked
      estimate
      remaining
      effort
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateBoard = /* GraphQL */ `
  subscription OnCreateBoard(
    $filter: ModelSubscriptionBoardFilterInput
    $owner: String
  ) {
    onCreateBoard(filter: $filter, owner: $owner) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateBoard = /* GraphQL */ `
  subscription OnUpdateBoard(
    $filter: ModelSubscriptionBoardFilterInput
    $owner: String
  ) {
    onUpdateBoard(filter: $filter, owner: $owner) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteBoard = /* GraphQL */ `
  subscription OnDeleteBoard(
    $filter: ModelSubscriptionBoardFilterInput
    $owner: String
  ) {
    onDeleteBoard(filter: $filter, owner: $owner) {
      id
      Columns {
        items {
          id
          boardID
          Tasks {
            items {
              id
              columnID
              name
              description
              blocked
              estimate
              remaining
              effort
              order
              createdAt
              updatedAt
              owner
            }
            nextToken
          }
          name
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onCreateColumn = /* GraphQL */ `
  subscription OnCreateColumn(
    $filter: ModelSubscriptionColumnFilterInput
    $owner: String
  ) {
    onCreateColumn(filter: $filter, owner: $owner) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onUpdateColumn = /* GraphQL */ `
  subscription OnUpdateColumn(
    $filter: ModelSubscriptionColumnFilterInput
    $owner: String
  ) {
    onUpdateColumn(filter: $filter, owner: $owner) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
export const onDeleteColumn = /* GraphQL */ `
  subscription OnDeleteColumn(
    $filter: ModelSubscriptionColumnFilterInput
    $owner: String
  ) {
    onDeleteColumn(filter: $filter, owner: $owner) {
      id
      boardID
      Tasks {
        items {
          id
          columnID
          name
          description
          blocked
          estimate
          remaining
          effort
          order
          createdAt
          updatedAt
          owner
        }
        nextToken
      }
      name
      order
      createdAt
      updatedAt
      owner
    }
  }
`;
