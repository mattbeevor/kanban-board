export enum inputType {
  'text',
  'textarea',
  'number',
  'boolean'
}
