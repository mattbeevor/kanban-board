import { API, graphqlOperation } from 'aws-amplify';
import { createColumn } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Board, Column } from '../models/index';
import { ColumnFields } from '../interfaces';

export default function useCreateColumn() {
  const queryClient = useQueryClient();

  interface columnProps {
    columnFields: ColumnFields;
    board: Board;
  }

  return useMutation({
    mutationFn: async ({ board, columnFields }: columnProps) => {
      const columns: Column[] = queryClient.getQueryData([board.id]) || [];

      return await API.graphql(
        graphqlOperation(createColumn, {
          input: {
            ...columnFields,
            boardID: board.id,
            order: columns.length
          }
        })
      );
    },

    onSuccess: (_, { board }: columnProps) => {
      queryClient.invalidateQueries({ queryKey: [board.id] });
    }
  });
}
