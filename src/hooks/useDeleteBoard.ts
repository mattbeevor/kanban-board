import { API, graphqlOperation } from 'aws-amplify';
import { deleteBoard } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';

export default function useDeleteBoard({
  user
}: {
  user: { id: string; username: string };
}) {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: async ({ id }: { id: string }) =>
      await API.graphql(
        graphqlOperation(deleteBoard, {
          input: {
            id
          }
        })
      ),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: [user.username] });
    }
  });
}
