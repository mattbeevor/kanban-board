import { useMutation, useQueryClient } from '@tanstack/react-query';
import updateListOfTasks from './updateListOfTasks';
import { useState } from 'react';
import { Task, Column } from '../models/index';

interface MousePosition {
  x: number;
  y: number;
}

interface ShiftTaskParams {
  tasks: Task[];
  targetColumn: Column;
  targetTask?: Task;
}

export default function useShiftTask() {
  const queryClient = useQueryClient();

  const [dragging, setDragging] = useState<{
    task: Task;
    tasks: Task[];
    offset: { x: number; y: number };
  } | null>(null);
  const [mousePos, setMousePos] = useState<MousePosition>({ x: 0, y: 0 });

  const shiftTaskMutation = useMutation({
    mutationFn: async ({
      tasks,
      targetColumn,
      targetTask
    }: ShiftTaskParams) => {
      if (dragging === null) {
        return;
      } else {
        setDragging(null);
      }

      const fromColumnId = dragging.task.columnID;
      const toColumnId = targetColumn.id;
      const movedTaskId = dragging.task.id;

      const newOrder = targetTask ? targetTask.order - 0.5 : Infinity;
      if (fromColumnId === toColumnId) {
        let list: Task[] = [...dragging.tasks];
        const draggedTask: Task | undefined = list.find(
          task => task.id === movedTaskId
        );
        if (draggedTask) {
          list = list.filter(t => t.id !== draggedTask.id);
          list.push({ ...draggedTask, order: newOrder });
        }

        return Promise.all([
          updateListOfTasks({
            queryClient,
            tasks: list,
            columnID: fromColumnId
          })
        ]);
      } else {
        const draggedTask = { ...dragging.task };
        draggedTask.columnID = toColumnId;
        draggedTask.order = newOrder;

        return Promise.all([
          updateListOfTasks({
            queryClient,
            tasks: dragging.tasks.filter(task => task.id !== movedTaskId),
            columnID: fromColumnId
          }),
          updateListOfTasks({
            queryClient,
            tasks: [...tasks, draggedTask],
            columnID: toColumnId
          })
        ]);
      }
    },
    onError: (_, { targetColumn }) => {
      if (dragging !== null) {
        queryClient.invalidateQueries({ queryKey: [dragging.task.columnID] });
      }
      queryClient.invalidateQueries({ queryKey: [targetColumn.id] });
    }
  });

  return {
    shiftTask: (args: ShiftTaskParams) => {
      if (dragging) {
        shiftTaskMutation.mutate(args);
      }
    },
    dragging,
    setDragging,
    mousePos,
    setMousePos
  };
}
