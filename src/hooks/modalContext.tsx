// MyContext.js
import React, { ReactNode, createContext, useState } from 'react';

//type modalType = {
//  key: string;
//  node: ReactNode;
//};

type contextType = {
  modals: ReactNode[];
  setModals: (arg0: any) => void;
  popModal: () => void;
  pushModal: (arg0: any) => void;
  clearModals: () => void;
};

const defaultContext: contextType = {
  modals: [],
  setModals: value => {},
  pushModal: value => {},
  clearModals: () => {},
  popModal: () => {}
};

export const ModalContext = createContext(defaultContext);

export const ModalContextProvider = ({ children }: { children: ReactNode }) => {
  const defaultArr: ReactNode[] = [];
  const [modals, setModals] = useState(defaultArr);

  const popModal = () => {
    setModals(modals => [...modals.slice(0, -1)]);
  };

  const pushModal = (modal: ReactNode) => {
    setModals(modals => [...modals, modal]);
  };

  const clearModals = () => {
    setModals([]);
  };

  return (
    <ModalContext.Provider
      value={{ modals, setModals, popModal, pushModal, clearModals }}
    >
      {children},{modals.map(modal => modal)}
    </ModalContext.Provider>
  );
};
