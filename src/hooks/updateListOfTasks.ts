import { API, graphqlOperation } from 'aws-amplify';
import { updateTask } from '../graphql/mutations';
import { Task } from '../models';

const updateListOfTasks = async ({
  queryClient,
  tasks,
  columnID
}: {
  queryClient: any;
  tasks: Array<Task>;
  columnID: string;
}) => {
  const normalizedTasks = tasks
    .sort((a, b) => a.order - b.order)
    .map((task, i) => ({ ...task, order: i }));

  queryClient.setQueryData([columnID], normalizedTasks);

  return Promise.all(
    normalizedTasks.map(
      async ({ columnID, id, order }) =>
        await API.graphql(
          graphqlOperation(updateTask, {
            input: {
              id,
              order,
              columnID
            }
          })
        )
    )
  );
};

export default updateListOfTasks;
