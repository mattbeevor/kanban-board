import { API, graphqlOperation } from 'aws-amplify';
import { updateTask } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { TaskFields } from '../interfaces';
import { Task } from '../models';

export default function useUpdateTask() {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: async ({
      task,
      taskFields
    }: {
      task: Task;
      taskFields: TaskFields;
    }) => {
      queryClient.setQueryData([task.id], taskFields.description);
      const tasks: Task[] = queryClient.getQueryData([task.columnID]) || [];
      queryClient.setQueryData(
        [task.columnID],
        tasks.map(task => {
          return { ...task, ...taskFields };
        })
      );

      await API.graphql(
        graphqlOperation(updateTask, {
          input: {
            ...taskFields,
            id: task.id
          }
        })
      );
    },
    onSuccess: async (_, { task }: { task: Task }) => {
      queryClient.invalidateQueries({ queryKey: [task.columnID] });
    },
    onError: (_, { task }: { task: Task }) => {
      queryClient.invalidateQueries({ queryKey: [task.id] });
    }
  });
}
