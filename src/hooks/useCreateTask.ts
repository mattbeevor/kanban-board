import { API, graphqlOperation } from 'aws-amplify';
import { createTask } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { TaskFields } from '../interfaces';

interface Column {
  id: string;
}

export default function useCreateTask() {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: async ({
      order,
      column,
      taskFields
    }: {
      order: number;
      column: Column;
      taskFields: TaskFields;
    }) =>
      await API.graphql(
        graphqlOperation(createTask, {
          input: {
            ...taskFields,
            columnID: column.id,
            order
          }
        })
      ),

    onSuccess: (_, { column }) => {
      queryClient.invalidateQueries({ queryKey: [column.id] });
    }
  });
}
