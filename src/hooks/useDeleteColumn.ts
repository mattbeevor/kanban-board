import { API, graphqlOperation } from 'aws-amplify';
import { deleteColumn } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Column } from '../models/index';
import updateListOfColumns from './updateListOfColumns';

export default function useDeleteColumn() {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: async ({ column }: { column: Column }) => {
      await API.graphql(
        graphqlOperation(deleteColumn, {
          input: {
            id: column.id
          }
        })
      );
      let columns: Column[] = queryClient.getQueryData([column.boardID]) || [];
      columns = columns.filter(columnInArray => columnInArray.id !== column.id);

      queryClient.setQueryData([column.boardID], columns);
      updateListOfColumns({ queryClient, columns, boardID: column.boardID });
    }
  });
}
