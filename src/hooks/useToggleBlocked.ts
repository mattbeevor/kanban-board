import { API, graphqlOperation } from 'aws-amplify';
import { updateTask } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Task } from '../models/index';

export default function useToggleBlocked() {
  const queryClient = useQueryClient();

  const getTasks = (task: Task): Task[] =>
    queryClient.getQueryData([task.columnID]) || [];

  const getList = (task: Task, blocked: boolean): Task[] =>
    getTasks(task).map(
      (t: Task): Task => (t.id === task.id ? { ...t, blocked } : t)
    );

  return useMutation({
    mutationFn: async ({ task }: { task: Task }) => {
      const blocked = !Boolean(task.blocked);

      const list = getList(task, blocked);

      queryClient.setQueryData([task.columnID], list);

      await API.graphql(
        graphqlOperation(updateTask, {
          input: {
            id: task.id,
            blocked
          }
        })
      );
    },
    onError: (_, { task }: { task: Task }) => {
      queryClient.invalidateQueries({ queryKey: [task.columnID] });
    }
  });
}
