import { API, graphqlOperation } from 'aws-amplify';
import { updateTask } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Task } from '../models/index';

export default function useUpdateEffort() {
  const queryClient = useQueryClient();

  const getTasks = (task: Task): Task[] =>
    queryClient.getQueryData([task.columnID]) || [];

  const getList = ({
    task,
    estimate,
    effort
  }: {
    task: Task;
    estimate: number;
    effort: number;
  }): Task[] =>
    getTasks(task).map(
      (t: Task): Task => (t.id === task.id ? { ...t, estimate, effort } : t)
    );

  return useMutation({
    mutationFn: async ({
      task,
      effort,
      estimate
    }: {
      task: Task;
      effort: number;
      estimate: number;
    }) => {
      const clamp = (x: number) => Math.max(0, Math.min(999, x));
      effort = clamp(effort);
      estimate = clamp(estimate);

      const list = getList({ task, effort, estimate });

      queryClient.setQueryData([task.columnID], list);

      await API.graphql(
        graphqlOperation(updateTask, {
          input: {
            id: task.id,
            effort,
            estimate
          }
        })
      );
    },
    onError: (_, { task }: { task: Task }) => {
      queryClient.invalidateQueries({ queryKey: [task.columnID] });
    }
  });
}
