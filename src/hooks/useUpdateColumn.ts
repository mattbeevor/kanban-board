import { API, graphqlOperation } from 'aws-amplify';
import { updateColumn } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { ColumnFields } from '../interfaces';
import { Column } from '../models/index';

export default function useUpdateColumn() {
  const queryClient = useQueryClient();

  interface columnProps {
    columnFields: ColumnFields;
    column: Column;
  }

  return useMutation({
    mutationFn: async ({ column, columnFields }: columnProps) =>
      await API.graphql(
        graphqlOperation(updateColumn, {
          input: {
            ...columnFields,
            id: column.id
          }
        })
      ),
    onSuccess: (_, { column }: columnProps) => {
      queryClient.invalidateQueries({ queryKey: [column.boardID] });
    }
  });
}
