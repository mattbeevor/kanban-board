import { API, graphqlOperation } from 'aws-amplify';
import { updateBoard } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { BoardFields, UserType } from '../interfaces';
import { Board } from '../models';

export default function useUpdateBoard() {
  const queryClient = useQueryClient();

  interface boardProps {
    boardFields: BoardFields;
    user: UserType;
    board: Board;
  }

  return useMutation({
    mutationFn: async ({ board, boardFields }: boardProps) => {
      await API.graphql(
        graphqlOperation(updateBoard, {
          input: {
            ...boardFields,
            id: board.id
          }
        })
      );
    },
    onSuccess: (_, { user }: boardProps) => {
      queryClient.invalidateQueries({ queryKey: [user.username] });
    }
  });
}
