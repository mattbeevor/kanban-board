import { API, graphqlOperation } from 'aws-amplify';
import { createBoard } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { BoardFields, UserType } from '../interfaces';

export default function useCreateBoard() {
  const queryClient = useQueryClient();

  interface boardProps {
    boardFields: BoardFields;
    user: UserType;
  }

  return useMutation({
    mutationFn: async ({ boardFields }: boardProps) =>
      await API.graphql(
        graphqlOperation(createBoard, {
          input: {
            ...boardFields
          }
        })
      ),
    onSuccess: (_, { user }: boardProps) => {
      queryClient.invalidateQueries({ queryKey: [user.username] });
    }
  });
}
