import { ModalContext } from './modalContext';
import { useContext } from 'react';

export default function useModal() {
  return useContext(ModalContext);
}
