import { API, graphqlOperation } from 'aws-amplify';
import { deleteTask } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import updateListOfTasks from './updateListOfTasks';
import { Task } from '../models/index';

export default function useDeleteTask() {
  const queryClient = useQueryClient();

  const getTasks = (id: string): Task[] => queryClient.getQueryData([id]) || [];

  return useMutation({
    mutationFn: async ({ task }: { task: Task }) =>
      Promise.all([
        await updateListOfTasks({
          queryClient,
          tasks: getTasks(task.columnID).filter(({ id }) => id !== task.id),
          columnID: task.columnID
        }),
        await API.graphql(
          graphqlOperation(deleteTask, {
            input: {
              id: task.id
            }
          })
        )
      ]),
    onSuccess: (_, { task }) => {
      queryClient.invalidateQueries({ queryKey: [task.columnID] });
    }
  });
}
