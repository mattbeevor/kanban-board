import { API, graphqlOperation } from 'aws-amplify';
import { updateColumn } from '../graphql/mutations';
import { Column } from '../models';

const updateListOfColumns = async ({
  queryClient,
  columns,
  boardID
}: {
  queryClient: any;
  columns: Array<Column>;
  boardID: string;
}) => {
  const normalizedColumns = columns
    .sort((a, b) => a.order - b.order)
    .map((task, i) => ({ ...task, order: i }));

  queryClient.setQueryData([boardID], normalizedColumns);

  return Promise.all(
    normalizedColumns.map(
      async ({ boardID, id, order }) =>
        await API.graphql(
          graphqlOperation(updateColumn, {
            input: {
              id,
              order,
              boardID
            }
          })
        )
    )
  );
};

export default updateListOfColumns;
