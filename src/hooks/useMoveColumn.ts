import { API, graphqlOperation } from 'aws-amplify';
import { updateColumn } from '../graphql/mutations';
import { useMutation, useQueryClient } from '@tanstack/react-query';
import { Column } from '../models/index';

export default function useMoveColumn() {
  const queryClient = useQueryClient();

  return useMutation({
    mutationFn: async ({
      column,
      direction
    }: {
      column: Column;
      direction: number;
    }) => {
      const list: Array<Column> =
        queryClient.getQueryData([column.boardID]) || [];

      const order = column.order;
      const swapOrder = column.order + direction;
      const swapColumn = list[swapOrder];

      if (!swapColumn) {
        return;
      }

      return Promise.all([
        await API.graphql(
          graphqlOperation(updateColumn, {
            input: {
              id: column.id,
              order: swapOrder
            }
          })
        ),
        await API.graphql(
          graphqlOperation(updateColumn, {
            input: {
              id: swapColumn.id,
              order: order
            }
          })
        )
      ]);
    },
    onSuccess: (_, { column }) => {
      queryClient.invalidateQueries({ queryKey: [column.boardID] });
    }
  });
}
