import { Amplify } from 'aws-amplify';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { withAuthenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';
import awsExports from './aws-exports';
import { ReactQueryDevtools } from '@tanstack/react-query-devtools';
import Router from './components/organisms/router';
import { ThemeProvider } from '@aws-amplify/ui-react';
import colors from 'tailwindcss/colors';
import { ModalContextProvider } from './hooks/modalContext';

Amplify.configure(awsExports);
const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      staleTime: 5 * (60 * 1000), // 5 mins
      cacheTime: 10 * (60 * 1000) // 10 mins
    }
  }
});

const theme = {
  name: 'Auth Example Theme',
  typography: {
    fontFamily: 'inherit' // Use the browser default font
  },
  tokens: {
    colors: {
      background: {
        primary: {
          value: colors.white[600]
        },
        secondary: {
          value: colors.blue[100]
        }
      },
      font: {
        interactive: {
          value: colors.black[600]
        }
      },
      brand: {
        primary: {
          10: colors.blue[100],
          80: colors.blue[400],
          90: colors.blue[500],
          100: colors.blue[500]
        }
      }
    },
    components: {
      tabs: {
        item: {
          _focus: {
            color: {
              value: colors.black[500]
            }
          },
          _hover: {
            color: {
              value: colors.black[500]
            }
          },
          _active: {
            color: {
              value: colors.black[500]
            }
          }
        }
      }
    }
  }
};

export default function App() {
  return (
    <ThemeProvider theme={theme}>
      {withAuthenticator(({ signOut, user }) => (
        <QueryClientProvider client={queryClient}>
          <ReactQueryDevtools></ReactQueryDevtools>
          <ModalContextProvider>
            <Router signOut={signOut} user={user}></Router>
          </ModalContextProvider>
        </QueryClientProvider>
      ))()}
    </ThemeProvider>
  );
}
