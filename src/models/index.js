// @ts-check
import { initSchema } from '@aws-amplify/datastore';
import { schema } from './schema';



const { Task, Board, Column } = initSchema(schema);

export {
  Task,
  Board,
  Column
};