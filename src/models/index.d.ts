import { ModelInit, MutableModel, __modelMeta__, ManagedIdentifier } from "@aws-amplify/datastore";
// @ts-ignore
import { LazyLoading, LazyLoadingDisabled, AsyncCollection } from "@aws-amplify/datastore";





type EagerTask = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Task, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly columnID: string;
  readonly name: string;
  readonly description: string;
  readonly blocked: boolean;
  readonly estimate?: number | null;
  readonly effort?: number | null;
  readonly order: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

type LazyTask = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Task, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly columnID: string;
  readonly name: string;
  readonly description: string;
  readonly blocked: boolean;
  readonly estimate?: number | null;
  readonly effort?: number | null;
  readonly order: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

export declare type Task = LazyLoading extends LazyLoadingDisabled ? EagerTask : LazyTask

export declare const Task: (new (init: ModelInit<Task>) => Task) & {
  copyOf(source: Task, mutator: (draft: MutableModel<Task>) => MutableModel<Task> | void): Task;
}

type EagerBoard = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Board, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly Columns?: (Column | null)[] | null;
  readonly name: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

type LazyBoard = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Board, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly Columns: AsyncCollection<Column>;
  readonly name: string;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

export declare type Board = LazyLoading extends LazyLoadingDisabled ? EagerBoard : LazyBoard

export declare const Board: (new (init: ModelInit<Board>) => Board) & {
  copyOf(source: Board, mutator: (draft: MutableModel<Board>) => MutableModel<Board> | void): Board;
}

type EagerColumn = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Column, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly boardID: string;
  readonly Tasks?: (Task | null)[] | null;
  readonly name: string;
  readonly order: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

type LazyColumn = {
  readonly [__modelMeta__]: {
    identifier: ManagedIdentifier<Column, 'id'>;
    readOnlyFields: 'createdAt' | 'updatedAt';
  };
  readonly id: string;
  readonly boardID: string;
  readonly Tasks: AsyncCollection<Task>;
  readonly name: string;
  readonly order: number;
  readonly createdAt?: string | null;
  readonly updatedAt?: string | null;
}

export declare type Column = LazyLoading extends LazyLoadingDisabled ? EagerColumn : LazyColumn

export declare const Column: (new (init: ModelInit<Column>) => Column) & {
  copyOf(source: Column, mutator: (draft: MutableModel<Column>) => MutableModel<Column> | void): Column;
}